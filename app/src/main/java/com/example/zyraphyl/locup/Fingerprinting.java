package com.example.zyraphyl.locup;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

public class Fingerprinting extends AppCompatActivity implements View.OnClickListener {
    private static final  int RESULT_LOAD_IMAGE = 1;

    private EditText row,column,filename;
    private ImageView select_image;
    private Button gather;
    private String path;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprinting);

        initialize();
    }
    private void initialize(){
        row = (EditText) findViewById(R.id.row);
        column = (EditText) findViewById(R.id.column);
        filename = (EditText) findViewById(R.id.filename);
        select_image = (ImageView) findViewById(R.id.select_image);
        select_image.setOnClickListener(this);
        gather = (Button) findViewById(R.id.gather);
        gather.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.select_image:
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent,RESULT_LOAD_IMAGE);
                break;
            case R.id.gather:
                Gather();
                break;
        }
    }
    private void Gather(){
        if(path!=null){
            String row_size = row.getText().toString();
            String column_size = column.getText().toString();
            String Filename = filename.getText().toString();

            if(!column_size.isEmpty() && !row_size.isEmpty() && !Filename.isEmpty()){
                Intent nextActivityIntent = new Intent(Fingerprinting.this, GatherSignal.class);
                nextActivityIntent.putExtra("path",path);
                nextActivityIntent.putExtra("rows",row_size);
                nextActivityIntent.putExtra("columns",column_size);
                nextActivityIntent.putExtra("filename",Filename);
                startActivity(nextActivityIntent);
            }
        }
    }
    public String getRealPathFromURI(Uri contentUri) {

        // can post image
        String [] proj={MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery( contentUri,
                proj, // Which columns to return
                null,       // WHERE clause; which rows to return (all rows)
                null,       // WHERE clause selection arguments (none)
                null); // Order-by clause (ascending by name)
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null){
            Uri selectedImage = data.getData();
            select_image.setImageURI(selectedImage);
            path = getRealPathFromURI(selectedImage);
        }
    }
}
