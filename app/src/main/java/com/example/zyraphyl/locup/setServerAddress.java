package com.example.zyraphyl.locup;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

public class setServerAddress extends AppCompatActivity implements View.OnClickListener {
    public SharedPreferences sharedPref;
    private Button database,localize;
    private EditText databaseAddress,localizeAddress;
    private int counterD=0,counterL=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_server_address);
        sharedPref = getApplicationContext().getSharedPreferences("SERVER", 0);
        initialize();
        databaseAddress.setText(sharedPref.getString("Server_Address",""));
        localizeAddress.setText(sharedPref.getString("Localize_Address",""));
    }
    public void initialize(){
        database = (Button) findViewById(R.id.changeDB);
        database.setOnClickListener(this);
        localize = (Button) findViewById(R.id.changelocalizeAddress);
        localize.setOnClickListener(this);
        databaseAddress = (EditText) findViewById(R.id.DBAddress);
        localizeAddress = (EditText) findViewById(R.id.localizeAddress);
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.changeDB:
                changeDBServer(String.valueOf(databaseAddress.getText()));
                counterD++;
                if(counterD%2==0){
                    databaseAddress.setEnabled(false);
                }else{
                    databaseAddress.setEnabled(true);
                }
                break;
            case R.id.changelocalizeAddress:
                changeLocalizeAddress(String.valueOf(localizeAddress.getText()));
                counterL++;
                if(counterL%2==0){
                    localizeAddress.setEnabled(false);
                }else{
                    localizeAddress.setEnabled(true);
                }
                break;
        }
    }
    public void changeDBServer(String server){

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Server_Address", server);
        editor.apply();
    }
    public void changeLocalizeAddress(String server){

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Localize_Address", server);
        editor.apply();
    }


}
