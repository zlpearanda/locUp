package com.example.zyraphyl.locup;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;
import android.content.IntentFilter;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GatherSignal extends AppCompatActivity implements View.OnClickListener {
    private int rows,columns,image_dimension,position,Rsslevel;
    private JSONObject wifi_info,Dataset;
    private ArrayList<Bitmap> chunked_images;
    private ArrayList<String> wifi_results;
    private ArrayAdapter<String> adapter;
    private WifiReceiver wifi_receiver;
    private List<ScanResult> wifi_list;
    private WifiManager wifi_manager;
    private ListView results_view;
    private JSONArray positions;
    private WifiInfo info;
    private float width;
    private String path,filename;
    private Button done;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gather_signal);

        path=getIntent().getStringExtra("path");
        rows = Integer.parseInt(getIntent().getStringExtra("rows"));
        columns = Integer.parseInt(getIntent().getStringExtra("columns"));
        filename = getIntent().getStringExtra("filename");
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        width = displayMetrics.widthPixels / displayMetrics.density;
        image_dimension = (int)width/columns;
        GatherSignal();
        results_view = (ListView) findViewById(R.id.results_view);
        wifi_results = new ArrayList<String>();

        adapter = new ArrayAdapter<String>(this,R.layout.list_signal,R.id.text1,wifi_results);
        results_view.setAdapter(adapter);
        wifi_manager = (WifiManager)this.getApplicationContext().getSystemService(this.WIFI_SERVICE);
        info = wifi_manager.getConnectionInfo();
        wifi_receiver = new WifiReceiver();

        done = (Button) findViewById(R.id.done_button);
        done.setOnClickListener(this);

        Dataset = new JSONObject();
        i=0;
    }
    private void GatherSignal(){
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        splitImage(bitmap);

        GridView image_grid = (GridView) findViewById(R.id.grid_view);

        image_grid.setAdapter(new ImageAdapter(this, chunked_images, image_dimension,this));
        image_grid.setNumColumns(columns);

    }
    private void splitImage(Bitmap image){

        int chunkHeight,chunkWidth;
        chunked_images = new ArrayList<Bitmap>(rows*columns);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(image, image.getWidth(), image.getHeight(), true);

        chunkHeight = image.getHeight()/rows;
        chunkWidth = image.getWidth()/columns;

        int yCoord = 0;
        for(int x=0; x<rows; x++){
            int xCoord = 0;
            for(int y=0; y<columns; y++){
                chunked_images.add(Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight));
                xCoord += chunkWidth;
            }
            yCoord += chunkHeight;
        }

    }
    public void scanWifi(int position){
        this.position = position;
        wifi_manager.startScan();
        Log.wtf("GatherSignal","scanWifi");

    }
    protected void onResume(){
        super.onResume();
        // Register wifi receiver to get the results
        registerReceiver(wifi_receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

    }

    protected void onPause(){
        super.onPause();
        // Unregister the wifi receiver
        unregisterReceiver(wifi_receiver);

    }


    @Override
    public void onClick(View v) {

        try{
            FileWriter file = new FileWriter(Environment.getExternalStorageDirectory()+"/"+filename+".json");
            file.write(Dataset.toString());
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        finish();
    }

    class WifiReceiver extends android.content.BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {

            wifi_results.clear();
            i++;
            positions = new JSONArray();
            wifi_list = wifi_manager.getScanResults();
            int j=0;
            for (ScanResult result : wifi_list) {
//                Rsslevel = WifiManager.calculateSignalLevel(result.level,100);
                wifi_results.add(result.toString());
                wifi_info = new JSONObject();
                String BSSID =result.BSSID;
                try {
                    wifi_info.put("SSID", result.SSID);
                    wifi_info.put("BSSID",BSSID);
                    wifi_info.put("RSS",result.level);
                    wifi_info.put("GridNumber",position);
                    wifi_info.put("Iteration",i);
                    positions.put(j++,wifi_info);
                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
            try {

                Dataset.put("Grid".concat(String.valueOf(position)).concat("-").concat(String.valueOf(i)),positions);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            adapter.notifyDataSetChanged();


            if (i%10!=0){
                scanWifi(position);
            }else{
                Toast.makeText(GatherSignal.this,"Done",Toast.LENGTH_SHORT).show();
            }
        }
    }
}