package com.example.zyraphyl.locup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public SharedPreferences sharedPref;
    private Button fingerprint,database,localize,server;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPref = getApplicationContext().getSharedPreferences("SERVER", 0);
        initialize();
    }
    private void initialize(){
        fingerprint = (Button) findViewById(R.id.fingerprint);
        fingerprint.setOnClickListener(this);
        database = (Button) findViewById(R.id.radioMap);
        database.setOnClickListener(this);
        localize = (Button) findViewById(R.id.Localize);
        localize.setOnClickListener(this);
        server = (Button) findViewById(R.id.Server);
        server.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Intent changeServer = new Intent(MainActivity.this,setServerAddress.class);
        Intent localizeActivity = new Intent(MainActivity.this,Localize.class);
        Intent createMap = new Intent(MainActivity.this,RadioMapCreation.class);
        Intent createFingerprint = new Intent(MainActivity.this,Fingerprinting.class);
        switch(v.getId()){
            case R.id.fingerprint:
                startActivity(createFingerprint);
                break;
            case R.id.radioMap:
                startActivity(createMap);
                break;
            case R.id.Localize:
                startActivity(localizeActivity);
                break;
            case R.id.Server:
                startActivity(changeServer);
                break;
        }
    }
}
