package com.example.zyraphyl.locup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class Localize extends AppCompatActivity {

    private static final int PICKFILE_RESULT_CODE = 1;
    private SharedPreferences sharedPref;
    private String Address,Content,Response;
    private Button selectFile,send;
    private TextView resultView;
    private EditText filename;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_localize);
        initialize();

    }
    private void initialize(){
        sharedPref = getApplicationContext().getSharedPreferences("SERVER", 0);
        Address = sharedPref.getString("Localize_Address","");
        send = (Button) findViewById(R.id.send);
        selectFile = (Button) findViewById(R.id.select_File);
        resultView = (TextView) findViewById(R.id.Results);
        filename = (EditText) findViewById(R.id.filename);
        selectFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/octet-stream");
                startActivityForResult(intent,PICKFILE_RESULT_CODE);
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Response=sendData(Content);
                resultView.setText(Response);
            }
        });
    }
    private String sendData(String json){

        HttpPost post = new HttpPost(Address);
        try {
            Log.wtf("sendData","sent");
            StringEntity stringEntity = new StringEntity(json);
            post.setEntity(stringEntity);
            post.setHeader("Content-type","application/json");

            DefaultHttpClient client = new DefaultHttpClient();
            BasicResponseHandler handler = new BasicResponseHandler();

            String response = client.execute(post,handler);
            Log.wtf("sendData",response);
            return response;
        } catch (UnsupportedEncodingException e) {
            Log.d("SendingJson",e.toString());
        } catch (ClientProtocolException e) {
            Log.d("SendingJson",e.toString());
        } catch (IOException e) {
            Log.d("SendingJson",e.toString());
        }
        return "Unable to connect to server";
    }
    private String readTextFile(Uri uri){
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(getContentResolver().openInputStream(uri)));
            String line = "";

            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PICKFILE_RESULT_CODE && resultCode == RESULT_OK && data != null){
            Uri selectedFile = data.getData();
            Cursor returnCursor =
                    getContentResolver().query(selectedFile, null, null, null, null);
            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            returnCursor.moveToFirst();
            filename.setText(returnCursor.getString(nameIndex));
            Content = readTextFile(selectedFile);
        }
    }
}
