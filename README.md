# locUp


This android application is used for creating a database of location fingerprints and testing accuracy of localization using json files.


__Features__

1. Fingerprinting
   
    

    An image of the floor map of the location is needed for this feature. This feature will divide the image into your desired number of rows and columns
    and will ask for a name of the file where the collected fingerprints will be written.
2. Location Fingerprint Creation

    In this feature, information about the location is needed before sending a json file to the server. The json file to be selected here is one of the
    created json file in feature 1.
3. Localize

    A json file meant for testing is selected here and the reply from the server will contain the probable location calculated in the server.
4. Server Address

    This is for setting the server address for localization and location fingerprint creation.


__Built With__
* Android Studio

__Images__


![Dashboard of app]
(/Markdown Images/dashboard.png "Dashboard")
![Fingerprinting]
(/Markdown Images/fingerprinting.png "Fingerprint")
![Fingerprinting Collection]
(/Markdown Images/colllection.png "Fingerprint Collection")
![Localization]
(/Markdown Images/localize.png "Localization")
![Database Creation]
(/Markdown Images/radiomap.png "Location Fingerprint Creation")
![Server Address]
(/Markdown Images/serverAddress.png "Server Address")



